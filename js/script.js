// ---------------------Выбор контента взависимости от таба СЕКЦИЯ ТРИ-----------------------

const contentBoxes = document.querySelectorAll('.third-section-content-box');
const menuTabs = document.querySelectorAll('.third-section-tab-text');
let activeTab;

const chooseContent = () => {
    const listItem = event.target;
    if (activeTab) {
        activeTab.classList.remove('active');
    }

    contentBoxes.forEach((child) => {
        if (child.dataset.name === listItem.id) {
            child.style.display = 'grid';
            listItem.classList.add('active');
            return;
        }
        activeTab = listItem;
        child.style.display = 'none';
    });
};

menuTabs.forEach((tab) => {
    tab.addEventListener('click', chooseContent);
});


// ---------------------Фильтр картинок СЕКЦИЯ ПЯТЬ-----------------------

const designImageCards = document.querySelectorAll('.fifth-section-image-card');
const imageTabs = document.querySelectorAll('.fifth-section-tab-menu');
let activeImageTab;

const chooseImage = () => {
    const listItem = event.target;
    if (activeImageTab) {
        activeImageTab.classList.remove('active-image-tab');
    }

    designImageCards.forEach((child) => {
        if ((child.dataset.name === listItem.id && child.dataset.visible === 'yes') || (child.dataset.type === listItem.id && child.dataset.visible === 'yes')) {
            child.style.display = 'flex';
            listItem.classList.add('active-image-tab');
            return;
        }
        activeImageTab = listItem;
        child.style.display = 'none';
    });
};

imageTabs.forEach((tab) => {
    tab.addEventListener('click', chooseImage);
});


// ---------------------------------КНОПКА LOAD MORE------------------------------------------

document.getElementById('load-more').addEventListener('click', () => {
    console.dir(event.target);
    event.target.style.display = 'none';
    document.querySelectorAll('.hidden-image').forEach((image) => {
        image.style.display = 'flex';
        image.setAttribute('data-visible', 'yes');
    });
});


// -----------------------СЛАЙДЕР--------------------------------

let activeRound = document.querySelector('.round1');
const checkActive = document.querySelector('.move-round-top');
const ourRounds = document.querySelectorAll('.round');
const bigRound = document.querySelector('.people-photo');
const peopleNameText = document.querySelector('.people-name');
const peopleCareerText = document.querySelector('.people-career');
const peopleSayText = document.querySelector('.seventh-section-text');
const leftArrow = document.querySelector('.left-arrow');
const rightArrow = document.querySelector('.right-arrow');

function setMolen() {
    bigRound.className = '';
    bigRound.classList.add('people-photo');
    bigRound.classList.add('bg-molen');
    peopleCareerText.innerText = "Frontend nedoDeveloper";
    peopleNameText.innerText = "Oleksandr Shaporda";
    peopleSayText.innerText = 'С другой стороны начало повседневной работы по формированию позиции в значительной степени обуславливает создание соответствующий условий активизации. Идейные соображения высшего порядка, а также постоянное информационно-пропагандистское обеспечение нашей деятельности позволяет оценить значение дальнейших направлений развития.';
}

function setAnastasia() {
    bigRound.className = '';
    bigRound.classList.add('people-photo');
    bigRound.classList.add('bg-anastasia');
    peopleCareerText.innerText = "Course Coordinator";
    peopleNameText.innerText = "Anastasia G";
    peopleSayText.innerText = 'Равным образом постоянное информационно-пропагандистское обеспечение нашей деятельности представляет собой интересный эксперимент проверки новых предложений. Не следует, однако забывать, что укрепление и развитие структуры требуют определения и уточнения позиций, занимаемых участниками в отношении поставленных задач.';
}

function setRodya() {
    bigRound.className = '';
    bigRound.classList.add('people-photo');
    bigRound.classList.add('bg-rodya');
    peopleCareerText.innerText = "Offline Mentor";
    peopleNameText.innerText = "Prosto Rodya";
    peopleSayText.innerText = 'Значимость этих проблем настолько очевидна, что укрепление и развитие структуры позволяет оценить значение существенных финансовых и административных условий. Задача организации, в особенности же укрепление и развитие структуры в значительной степени обуславливает создание существенных финансовых и административных условий.';
}

function setYamnyk() {
    bigRound.className = '';
    bigRound.classList.add('people-photo');
    bigRound.classList.add('bg-yamnyk');
    peopleCareerText.innerText = "Луший Учытэл";
    peopleNameText.innerText = "Mister Kalampucai";
    peopleSayText.innerText = 'Значимость этих проблем настолько очевидна, что рамки и место обучения кадров позволяет оценить значение новых предложений. Идейные соображения высшего порядка, а также постоянное информационно-пропагандистское обеспечение нашей деятельности способствует подготовки и реализации форм развития. ';
}

function getProperContent() {
    switch (activeRound.dataset.photo) {
        case 'molen':
            setMolen();
            break;

        case 'anastasia':
            setAnastasia();
            break;

        case 'rodya':
            setRodya();
            break;

        case 'yamnyk':
            setYamnyk();
            break;
    }
}

leftArrow.addEventListener('click', () => {
    console.log(activeRound);
    if (!activeRound.previousElementSibling.classList.contains('move-round-top')) {
        activeRound.classList.remove('move-round-top');
        activeRound.previousElementSibling.classList.add('move-round-top');
        activeRound = activeRound.previousElementSibling;
        getProperContent();

        if (activeRound.previousElementSibling === null) {
            activeRound = document.querySelector('.round1');
        }
    }
});

rightArrow.addEventListener('click', () => {
    if (!activeRound.nextElementSibling.classList.contains('move-round-top')) {
        activeRound.classList.remove('move-round-top');
        activeRound.nextElementSibling.classList.add('move-round-top');
        activeRound = activeRound.nextElementSibling;
        getProperContent();
    }
});

ourRounds.forEach((round) => {
    round.addEventListener('click', () => {
            const roundItem = event.target;
            checkActive.classList.remove('move-round-top');
            if (activeRound) {
                activeRound.classList.remove('move-round-top');
            }
            round.classList.add('move-round-top');
            activeRound = roundItem;
            console.log(activeRound);

            getProperContent();
        }
    );
});

document.querySelectorAll('.link-icon').forEach((card)=>{
    card.addEventListener('mouseover', ()=>{
        card.classList.add('green-link-icon');
        card.innerHTML = "";
    });
});

document.querySelectorAll('.link-icon').forEach((card)=>{
    card.addEventListener('mouseout', ()=>{
        card.classList.remove('green-link-icon');
    });
});

document.querySelectorAll('.search-icon').forEach((card)=>{
    card.addEventListener('mouseover', ()=>{
        card.classList.add('green-search-icon');
    });
});

document.querySelectorAll('.search-icon').forEach((card)=>{
    card.addEventListener('mouseout', ()=>{
        card.classList.remove('green-search-icon');
    });
});